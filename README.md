ansible-role-grafana
=========

Configure Grafana. Create datasources and load dashboards.

Requirements
------------

```
ansible-galaxy collection install community.grafana
```

Role Variables
--------------

See defaults/main.yml.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

```yaml
- name: Grafana configuration
  hosts: localhost
  tasks:
    - include_role:
        name: grafana
        apply:
          tags: grafana
      tags: always
```

Author Information
------------------

Rudi Broekhuizen
